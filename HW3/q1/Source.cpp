#include <iostream>
#include "Vector.h"
using  namespace std;
void main()
{



	Vector v(6);
	cout << "size" << v.size() << ", capacity:" << v.capacity() << " resize factor" << v.resizeFactor() << std::endl;
	for (int i = 0; i < 20; i++)
	{
		v.push_back(i*i);
	}
	

	v.assign(6);
	
	cout << "size: " << v.size() << ", capacity: " << v.capacity() << "  resize factor:" << v.resizeFactor() << std::endl;
	v.resize(25, 25);
	
	system("pause");
}