#include "Vector.h"
#include <iostream>
//constractor
Vector::Vector(int n)
{
	if (n < 2)
		n = 2;
	_elements = new int[n];
	_capacity = n;
	_size = 0;
	_resizeFactor = n;
}
//deconstractor
Vector::~Vector()
{
	delete[] _elements;
	_capacity = 0;
}
//Get size
int Vector::  size() const 
{
	return _size;
}
//get capacity
int Vector::capacity() const

{
	return _capacity;
}
//get resize Factor
int Vector::resizeFactor() const
{
	return _resizeFactor;
}
//if array empty return true else return false
bool Vector::empty() const
{
	return _size ==0 ? true : false;
}
//add new element at end of array
void Vector::push_back(const int& val)
{
	int* tmp;
	int i = 0;
	
	if (_size < _capacity)
	{
		this->_elements[_size++] = val;
	}
	//case need to extened the capacity because the elements size
	else if (_size == _capacity)
	{
		

		tmp = new int[_size];
		for ( i = 0; i < _size; i++)
		{
			tmp[i] = this->_elements[i];
		}
		_elements = new int[_capacity+_resizeFactor];
		_capacity += _resizeFactor;
		for (i=0; i < _size; i++)
		{
			this->_elements[i] = tmp[i];
		}
		this->_elements[_size++]= val;
		
		delete[] tmp;
			
	}
}
//delete element from the end of array 
int Vector::pop_back()
{
	int tmp = 0;
	
	if (this->_size == 0)
	{
		std::cout << "error: pop from empty vector" << std::endl;
		return -9999;
	}

	else
	{
		_size--;
		tmp = this->_elements[_size];
		
		
		
	}
	return tmp;
}
//save /extend the array with n and considered resize factor
void Vector::reserve(int n)
{
	int* tmp;
	if (n <= _capacity)
	{
		return;
	}
	else if (n > _capacity)
	{
		tmp = new int[_size];
		for (int i = 0; i < _size; i++)
		{
			tmp[i] = this->_elements[i];
		}
		//calculate for how much increase the vector
		int cap = n - _capacity;
		cap /= _resizeFactor;
		cap += 1;
		//realloc the vector in new size
		_elements = new int[_resizeFactor*cap+_capacity];
		//set new capacity
		_capacity += _resizeFactor*cap;
		for (int i = 0; i < _size; i++)
		{
			this->_elements[i] = tmp[i];
		}
		

		delete[] tmp;
	}
}
//return to new size by resize factor and consider n
void Vector::resize(int n)
{
	if (n <= _capacity)
	{
		_size = n;
	}
	else if (n > _capacity)
	{
		this->reserve(n);
	}
}
//assign all the cells by given value
void Vector::assign(int val)
{
	for (int i = 0; i < _size; i++)
	{
		_elements[i] = val;
	}
}
//resize the vector using resize Factor and considered parameter n and put val in all the new cell until size = capacity
void Vector::resize(int n, const int& val)

{
	if (n <= _capacity)
	{
		_size = n;
	}
	else if (n > _capacity)
	{
		//to make the array bigger 
		this->reserve(n);
		for (int i = _size; i < _capacity; i++)
		{
			this->_elements[i] = val;
		}
	}
	_size = _capacity;

}
//deep copy constractor
Vector::Vector(const Vector& other)

{
	for (int i = 0; i < other.capacity(); i++)
	{
		this->_elements[i] = other._elements[i];
		

	}
	_capacity = other.capacity();
	this->_size = other.size();
	this->_resizeFactor = other.resizeFactor();
}

//reference/shallow constractor
Vector& Vector::operator=(const Vector& other)
{
	*this = other;
	return *this;
}
//acces to element using []
int& Vector::operator[](int n) const
{
	if (n > _size)
	{
		std::cout << " the paramet is out of range" << std::endl;
		return this->_elements[0];
	}
	return this->_elements[n];
}
